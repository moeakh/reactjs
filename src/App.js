
import './App.css';
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom";
import ProductList from './component/Pages/ProductList';
import AddProduct from './component/Pages/AddProduct';

function App() {
  return (
    <div className="App">
      <div className='container'>
      {/* This is the alias of BrowserRouter i.e. Router */}
      <Router>
        <Routes>

          <Route exact path="/" element={<ProductList/>} />

          <Route path="/add-product" element={<AddProduct/>} />

        </Routes>
      </Router>
      </div>
    </div>
  );
}

export default App;
