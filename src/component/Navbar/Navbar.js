import React from 'react'
import './Navbar.css'
import Button from '../Elements/button'
import PropTypes from 'prop-types'

const Navbar = ({link,Items}) => {
    return (
        <nav className='navbarItems'>
            <h1 className="logo">{link ? "Product List":"Add Product"} <i className={link ?"fa fa-shopping-basket":"fas fa-cart-plus"}></i></h1>
            <ul className='nav-menu'>

                {Items.map((item,index) => {return(
                    <li key={index}>
                        <div className={item.cName}>
                            <Button id={item.id} text={item.title} clicks={item.onClickNb} ></Button>
                        </div>
                    </li>
                )
                })}
            </ul>
        </nav>
    )
    
}
Navbar.propTypes ={
    link : PropTypes.bool,
    Items : PropTypes.array,
}

export default Navbar