export const ListItems = [
    {
        id:"ADD",
        title:"ADD",
        cName:"nav-links",
        onClickNb : 0
    },
    {
        id:"delete-product-btn",
        title:"MASS DELETE",
        cName:"nav-links",
        onClickNb : 1
    }
]

export const AddItems = [
    {
        id:'save-btn',
        title:"Save",
        cName:"nav-links",
        onClickNb : 2
    },
    {
        id:'cancel-btn',
        title:"Cancel",
        cName:"nav-links",
        onClickNb : 3
    }
]