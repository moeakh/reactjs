import React from 'react'
import {AddItems} from '../Navbar/NavItems'
import Navbar from'../Navbar/Navbar'
import EditText from '../Elements/EditText'
import ProductType from '../Elements/ProductType'
import {values} from '../Elements/CommonArrays'

const AddProduct = () => {
    
    return (
        <div>
            <Navbar link={false} Items={AddItems}/>
            <h1>Add your product</h1>
            <form id="product_form">
                {values.map((value,index)=> {return(
                    <div key={index}>
                        <EditText label={value.labelTitle} id={value.id}/>
                    </div>
                )})
                }
                <ProductType/>
            </form>
        </div>
    )
}

export default AddProduct
