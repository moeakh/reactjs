import React from 'react'
import {ListItems} from '../Navbar/NavItems'
import Navbar from'../Navbar/Navbar'
import ProductSpecs from '../Elements/ProductSpecs'

const ProductList = () => {
    return (
        <div>
            <Navbar link={true} Items={ListItems} />
            <ProductSpecs/>
        </div>
    )
}

export default ProductList
