import React from 'react'
import {useState} from 'react'
import './ProductType.css'
import EditText from './EditText'

const ProductType = () => {
    const [isActive,setisActive] = useState(false);
    const [text, setText] = useState("Chose one");
    const [ShowDVD, setShowDVD] = useState(false);
    const [ShowBook, setShowBook] = useState(false);
    const [ShowFurniture, setShowFurniture] = useState(false);

    function showdvd(){
        setShowDVD(true);
        setShowFurniture(false);
        setShowBook(false);
    }
    function showbook(){
        setShowDVD(false);
        setShowFurniture(false);
        setShowBook(true);
    }
    function showfurniture(){
        setShowDVD(false);
        setShowFurniture(true);
        setShowBook(false);
    }

    function selected(){
        console.log(document.getElementById('productType').value);
        var item = document.getElementById('productType').value;
        {item === "DVD" ? showdvd() :
            item === "Book" ? showbook() : showfurniture()
        }
    }
    return (
        <div>
            <div className='side'>
                <label className='col'>Product type</label>
                <select id='productType' className='col' onChange={selected}>
                    <option value=""></option>
                    <option value="DVD">DVD</option>
                    <option value="Book">Book</option>
                    <option value="Furniture">Furniture</option>
                </select>
            </div>
            <div>
                {ShowDVD && (<form id='DVD'>
                    <EditText label='Size in (MB) :' id='size'/>
                </form>
                )}
                {ShowBook && (<form id='Book'>
                    <EditText label='Weight in (Kg) :' id='weight'/>
                </form>
                )}
                {ShowFurniture && ( <form id='Furniture'>
                    <EditText label='Height CM:' id='height'/>
                    <EditText label='Width CM:' id='width'/>
                    <EditText label='Length CM:' id='length'/>
                </form>
                )}
            </div>
        </div>
    )
}

export default ProductType