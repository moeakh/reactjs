import React from 'react'
import './button.css'
import $ from 'jquery'

const button = ({id,text,clicks}) => {
    return (
        <button id={id} className='button' onClick={clicks ===3 ?cancel :clicks === 1 ? massDelete : clicks === 2 ? Save : zero}>{text}</button>
    )
}
function cancel(){
    window.location.assign("https://juniorwebdevtest.netlify.app")
}
function massDelete(){
    var toDelete = [];
    $("input:checkbox[name='delete-checkbox']:checked").each(function(){    
    toDelete.push($(this).attr("id"));            
    });
    for (let i = 0; i < toDelete.length; i++) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
        "id": toDelete[i]
        });

        var requestOptions = {
        method: 'DELETE',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
        };
        
        fetch("https://juniorweb-dev.000webhostapp.com/phpapitest/api/Product/delete.php", requestOptions)
        .then(response => response.text())
        .then(result => console.log(result))
        .catch(error => console.log('error', error));
    }
   alert("deleted");
   window.location.assign("https://juniorwebdevtest.netlify.app");
}
function generateSpecs(){
    const productType=$('#productType').val();
    var specs;
    const size = $('#size').val();
    const weight = $('#weight').val();
    const length = $('#length').val();
    const width = $('#width').val();
    const height = $('#height').val();
    console.log(productType);
    {
        productType === "DVD" ?
            specs = "Size in (MB) : "+size 
        :productType === "Book" ?
            specs = "Weight in (Kg) : "+weight 
        :productType === "Furniture" ?
            specs = "Dimensions : "+height+"x"+width+"x"+length
        :
            specs =""  
    }
    console.log(specs);
    return specs;
}
function Save(){
    {($('#sku').val()===""||$('#name').val()===""||$('#price').val()===""||$('#productType').val()==="Chose one"||generateSpecs()==="")
        ?  alert("fill all the required fields") : saved()
    }
}
function saved(){
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var raw = JSON.stringify({
      "sku": $('#sku').val(),
      "name": $('#name').val(),
      "price": $('#price').val(),
      "type": $('#productType').val(),
      "specs": generateSpecs()
    });  
    var requestOptions = {
      mode:'no-cors',
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    fetch("https://juniorweb-dev.000webhostapp.com/phpapitest/api/Product/create.php", requestOptions)
      .then(response => response.text())
      .then(result => console.log(result))
      .catch(error => console.log('error', error));
    alert("App added succefuly");
    window.location.assign("https://juniorwebdevtest.netlify.app");
}
const zero= ()=>{
    window.location.assign("https://juniorwebdevtest.netlify.app/add-product");
}

export default button
