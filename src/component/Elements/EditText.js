import React from 'react'

const EditText = ({label, id}) => {
    return (
        <div className='side'>
            <label className='col'>{label} </label>
            <input className='col' id={id} type="text" size={25}></input>
        </div>
    )
}

export default EditText
