import React from 'react'
import './ProductSpecs.css'
import Axios from 'axios'
import { useEffect, useState } from 'react'

const ProductSpecs = () => {

const [Data,setData]= useState([]);

    useEffect(()=>{
        Axios.get('https://juniorweb-dev.000webhostapp.com/phpapitest/api/Product/read.php')
        .then(res=>{
            console.log(res.data);
            setData(res.data.data);
    }).catch(err=>console.log(err))
    },[])

    const arr = Data.map((data,index)=>{
        return(
            <div key={index} className="responsive">
                <div className="box">
                    <input id={data.id} type="checkbox" name='delete-checkbox' className='delete-checkbox' ></input>
                    <div className='specs'>
                        <div>
                            <p>Name: {data.name}</p>
                            <p>SKU: {data.sku}</p>
                            <p>Price: {data.price} $</p>
                            <p>Type: {data.type}</p>
                            <p>{data.specs}</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    });

    return (
            <div className="row">
                {arr}
            </div>
    )
}

export default ProductSpecs
